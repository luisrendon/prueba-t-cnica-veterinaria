package com.example.veterinaria.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.veterinaria.R;
import com.example.veterinaria.models.ListItem;

import java.util.ArrayList;

public class OptionListAdapter extends RecyclerView.Adapter<OptionListAdapter.ViewHolderOptionListAdapter> implements View.OnClickListener {


    private ArrayList<ListItem> listItems;
    private View.OnClickListener listener;

    public OptionListAdapter(ArrayList<ListItem> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override

    public ViewHolderOptionListAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_option_list, parent, false);
        view.setOnClickListener(this);
        return new ViewHolderOptionListAdapter(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderOptionListAdapter holder, int position) {
        holder.lblOption.setText(listItems.get(position).getValue());

    }

    public void setOnclickListener(View.OnClickListener listener) {
        this.listener = listener;

    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolderOptionListAdapter extends RecyclerView.ViewHolder {
        TextView lblOption;

        public ViewHolderOptionListAdapter(@NonNull View itemView) {
            super(itemView);
            lblOption = itemView.findViewById(R.id.txt_option);

        }
    }


}
