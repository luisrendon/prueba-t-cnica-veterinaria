package com.example.veterinaria.Util;

public class Util {
    public static final String bdName = "db_veterinaria";
    public static final String CREATE_MASCOTA = "CREATE TABLE IF NOT EXISTS [MASCOTA](\n" +
            "    [CEDULA] [VARCHAR(15)] NOT NULL, \n" +
            "    [ID] INTEGER PRIMARY KEY ASC NOT NULL UNIQUE, \n" +
            "    [NOMBRE] [VARCHAR(120)] NOT NULL, \n" +
            "    [TIPO] INTEGER NOT NULL, \n" +
            "    [EDAD] INTEGER NOT NULL, \n" +
            "    [RAZA] [VACHAR(120)] NOT NULL, \n" +
            "    [ID_PROPIETARIO] INTEGER NOT NULL REFERENCES PROPIETARIO([ID]));";
    public static final String CREATE_PROPIETARIO = "CREATE TABLE IF NOT EXISTS[PROPIETARIO](\n" +
            "    [ID] INTEGER PRIMARY KEY ASC NOT NULL, \n" +
            "    [CEDULA] [VARCHAR(15)] NOT NULL, \n" +
            "    [NOMBRE] [VARCHAR(120)] NOT NULL, \n" +
            "    [TELEFONO] [VARCHAR(20)] NOT NULL);";

    public static final String CREATE_VACUNA = "CREATE TABLE IF NOT EXISTS[VACUNA](\n" +
            "    [ID] INTEGER PRIMARY KEY ASC NOT NULL, \n" +
            "    [NOMBRE] [VARCHAR(120)] NOT NULL, \n" +
            "    [FECHA] TIMESTAMP NOT NULL, \n" +
            "    [DOSIS] DOUBLE NOT NULL, \n" +
            "    [ID_MASCOTA] INTEGER NOT NULL REFERENCES VACUNA([ID_MASCOTA]));";


    public static final int RESP_OWNER_LIST = 1;
    public static final int RESP_TYPE_LIST = 2;
    public static final int RESP_PET_LIST = 3;
}
