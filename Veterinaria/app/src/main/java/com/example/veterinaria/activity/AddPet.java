package com.example.veterinaria.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.veterinaria.DBO.DbManager;
import com.example.veterinaria.R;
import com.example.veterinaria.Util.Util;
import com.example.veterinaria.models.ListItem;
import com.example.veterinaria.models.Propietario;

import java.util.ArrayList;

public class AddPet extends AppCompatActivity implements View.OnClickListener {
    private EditText txtName, txtId, txtAge, txtRace, txtOwner, txtType;
    private Button btnSelectOwner, btnSelectType, btnAddPet;
    private ArrayList<Propietario> ownerOptions;
    private int selectOwner;
    private int selectType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);
        intiComponetns();
    }

    /**
     *
     */
    public void intiComponetns() {
        selectOwner = -1;
        selectType = -1;
        txtName = findViewById(R.id.txt_id);
        txtId = findViewById(R.id.txt_name);
        txtAge = findViewById(R.id.txt_age);
        txtRace = findViewById(R.id.txt_race);
        txtOwner = findViewById(R.id.txt_owner);
        txtType = findViewById(R.id.txt_type);
        btnSelectOwner = findViewById(R.id.btn_select_owner);
        btnSelectType = findViewById(R.id.btn_select_type);
        btnAddPet = findViewById(R.id.btn_add_pet);
        btnSelectOwner.setOnClickListener(this);
        btnSelectType.setOnClickListener(this);
        btnAddPet.setOnClickListener(this);
        ownerOptions = new ArrayList<>();
        ownerOptions = DbManager.getOwner(AddPet.this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_owner:
                Intent intent = new Intent(AddPet.this, AddOptionsListActivity.class);
                intent.putExtra("LIST_ITEMS", loadOwnerList());
                startActivityForResult(intent, Util.RESP_OWNER_LIST);
                break;
            case R.id.btn_select_type:
                intent = new Intent(AddPet.this, AddOptionsListActivity.class);
                intent.putExtra("LIST_ITEMS", loadType());
                startActivityForResult(intent, Util.RESP_TYPE_LIST);
                break;
            case R.id.btn_add_pet:

                DbManager.addPet(txtId.getText().toString(), txtName.getText().toString(), selectType, Integer.parseInt(txtAge.getText().toString()), txtRace.getText().toString(), selectOwner, AddPet.this);
                intent = new Intent(AddPet.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    public ArrayList<ListItem> loadOwnerList() {
        ArrayList<ListItem> ownerList = new ArrayList<>();

        for (int i = 0; i < ownerOptions.size(); i++) {
            ownerList.add(new ListItem(ownerOptions.get(i).getId(), ownerOptions.get(i).getNombre()));
        }
        return ownerList;
    }

    public ArrayList<ListItem> loadType() {
        ArrayList<ListItem> typeList = new ArrayList<>();

        typeList.add(new ListItem(1, "Perro"));
        typeList.add(new ListItem(2, "Gato"));
        typeList.add(new ListItem(3, "Pájaro"));

        return typeList;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Util.RESP_OWNER_LIST:
                if (resultCode == RESULT_OK) {
                    ListItem itemObject = (ListItem) data.getExtras().getSerializable("ITEM_LIST_ACTIVITY");
                    selectOwner = itemObject.getId();
                    txtOwner.setText(itemObject.getValue());
                }
                break;

            case Util.RESP_TYPE_LIST:
                if (resultCode == RESULT_OK) {
                    ListItem itemObject = (ListItem) data.getExtras().getSerializable("ITEM_LIST_ACTIVITY");
                    selectType = itemObject.getId();
                    txtType.setText(itemObject.getValue());
                }
                break;

        }
    }
}
