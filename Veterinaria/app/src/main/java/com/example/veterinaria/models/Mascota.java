package com.example.veterinaria.models;

public class Mascota {
    private Integer id;
    private String cedula;
    private String nombre;
    private Integer tipo;
    private Integer edad;
    private String raza;
    private Integer idPropietario;

    public Mascota() {
    }

    public Mascota(Integer id, String cedula, String nombre, Integer tipo, Integer edad, String raza, Integer idPropietario) {
        this.id = id;
        this.cedula = cedula;
        this.nombre = nombre;
        this.tipo = tipo;
        this.edad = edad;
        this.raza = raza;
        this.idPropietario = idPropietario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public Integer getIdPropietario() {
        return idPropietario;
    }

    public void setIdPropietario(Integer idPropietario) {
        this.idPropietario = idPropietario;
    }
}
