package com.example.veterinaria.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.veterinaria.DBO.DbManager;
import com.example.veterinaria.R;

public class AddOwner extends AppCompatActivity implements View.OnClickListener {
    private EditText txtName, txtId, txtPhoneNumber;
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_owner);
        initComponents();
    }

    /**
     * Inicializa los componentes gráficos
     */
    public void initComponents() {
        txtName = findViewById(R.id.txt_name);
        txtId = findViewById(R.id.txt_id);
        txtPhoneNumber = findViewById(R.id.txt_phone);
        btnAdd = findViewById(R.id.btn_add_owner);
        btnAdd.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_owner:
                if (validateInfo()) {
                    DbManager.addOwner(txtId.getText().toString(), txtName.getText().toString(), txtPhoneNumber.getText().toString(), AddOwner.this);
                    Log.d("Debug", "tamaño lista " + DbManager.getOwner(AddOwner.this).size());
                    cleanForm();
                }
                Intent intent = new Intent(AddOwner.this, MainActivity.class);
                startActivity(intent);

                break;
        }
    }

    /**
     * valida los campos requeridos para el registro
     */
    public boolean validateInfo() {
        if (txtName.getText().toString().isEmpty() || txtId.getText().toString().isEmpty() || txtPhoneNumber.getText().toString().isEmpty()) {
            Toast.makeText(AddOwner.this, getString(R.string.val_required), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void cleanForm() {
        txtName.setText("");
        txtPhoneNumber.setText("");
        txtId.setText("");
    }

}
