package com.example.veterinaria.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.veterinaria.DBO.ConexionSqlHelper;
import com.example.veterinaria.R;
import com.example.veterinaria.Util.Util;
import com.example.veterinaria.models.ListItem;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnAddOwner, btnAddPet, btnAddVaccine, btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConexionSqlHelper conexionSqlHelper = new ConexionSqlHelper(this, "db_veterinaria", null, 1);
        initComponents();
    }

    /**
     * Método que inicializa los componentes gráficos
     */
    public void initComponents() {
        btnAddOwner = findViewById(R.id.btn_add_owner);
        btnAddPet = findViewById(R.id.btn_add_pet);
        btnAddVaccine = findViewById(R.id.btn_add_vaccine);
        btnSearch = findViewById(R.id.btn_search);

        btnSearch.setOnClickListener(this);
        btnAddOwner.setOnClickListener(this);
        btnAddPet.setOnClickListener(this);
        btnAddVaccine.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_owner:
                Intent intent = new Intent(MainActivity.this, AddOwner.class);
                startActivity(intent);
                break;
            case R.id.btn_add_pet:
                intent = new Intent(MainActivity.this, AddPet.class);
                startActivity(intent);
                break;
            case R.id.btn_add_vaccine:
                intent = new Intent(MainActivity.this, AddVaccine.class);
                startActivity(intent);
                break;
            case R.id.btn_search:
                intent = new Intent(MainActivity.this, SearchPetActivity.class);
                startActivity(intent);

                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Util.RESP_PET_LIST:
                if (resultCode == RESULT_OK) {
                    ListItem itemObject = (ListItem) data.getExtras().getSerializable("ITEM_LIST_ACTIVITY");
            /*        selectOwner = itemObject.getId();
                    txtOwner.setText(itemObject.getValue());*/
                }
                break;


        }
    }


}
