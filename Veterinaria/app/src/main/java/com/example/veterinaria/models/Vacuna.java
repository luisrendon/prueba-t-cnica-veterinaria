package com.example.veterinaria.models;

import java.util.Date;

public class Vacuna {

    private Integer id;
    private String nombre;
    private String fecha;
    private double dosis;
    private Integer idMascota;


    public Vacuna() {

    }

    public Vacuna(Integer id, String nombre, String fecha, double dosis, Integer idMascota) {
        this.id = id;
        this.nombre = nombre;
        this.fecha = fecha;
        this.dosis = dosis;
        this.idMascota = idMascota;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getDosis() {
        return dosis;
    }

    public void setDosis(double dosis) {
        this.dosis = dosis;
    }

    public Integer getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(Integer idMascota) {
        this.idMascota = idMascota;
    }
}
