package com.example.veterinaria.models;

import java.io.Serializable;

public class ListItem implements Serializable {
    private int id;
    private String value;

    public ListItem(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public ListItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
