package com.example.veterinaria.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.veterinaria.DBO.DbManager;
import com.example.veterinaria.R;

public class SearchPetActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtId;
    private TextView lblConsole;
    private Button btnSlectPet, btnDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_pet);
        initComponents();
    }

    public void initComponents() {
        txtId = findViewById(R.id.txt_id);
        lblConsole = findViewById(R.id.lbl_console);
        btnSlectPet = findViewById(R.id.btn_select_pet);
        btnDelete = findViewById(R.id.btn_delete);
        btnSlectPet.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_pet:
                lblConsole.setText(DbManager.getPetInfo(SearchPetActivity.this, txtId.getText().toString()));
                break;
            case R.id.btn_delete:
                DbManager.deletePet(txtId.getText().toString(), SearchPetActivity.this);
                break;
        }


    }
}
