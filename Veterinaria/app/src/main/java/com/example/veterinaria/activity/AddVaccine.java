package com.example.veterinaria.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.veterinaria.DBO.DbManager;
import com.example.veterinaria.R;
import com.example.veterinaria.Util.Util;
import com.example.veterinaria.models.ListItem;
import com.example.veterinaria.models.Mascota;
import com.example.veterinaria.models.Vacuna;

import java.util.ArrayList;

public class AddVaccine extends AppCompatActivity implements View.OnClickListener {

    private EditText txtName, txtDate, txtDose, txtPet;
    private Button btnSelectPet, btnAddVaccine;
    private ArrayList<Mascota> petOptions;
    private int selectedPet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vaccine);
        initComponets();
    }


    public void initComponets() {
        txtName = findViewById(R.id.txt_id);
        txtDate = findViewById(R.id.txt_date);
        txtDose = findViewById(R.id.txt_dose);
        txtPet = findViewById(R.id.txt_pet);
        btnSelectPet = findViewById(R.id.btn_select_pet);
        btnAddVaccine = findViewById(R.id.btn_add_vaccine);
        btnSelectPet.setOnClickListener(this);
        btnAddVaccine.setOnClickListener(this);
        petOptions = new ArrayList<>();
        petOptions = DbManager.getPet(AddVaccine.this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_pet:

                Intent intent = new Intent(AddVaccine.this, AddOptionsListActivity.class);
                intent.putExtra("LIST_ITEMS", loadOwnerList());
                startActivityForResult(intent, Util.RESP_PET_LIST);
                break;
            case R.id.btn_add_vaccine:
                DbManager.addVaccine(txtName.getText().toString(), txtDate.getText().toString(), Double.parseDouble(txtDose.getText().toString()), selectedPet, AddVaccine.this);
                ArrayList<Vacuna> vaccineList = DbManager.getVaccine(AddVaccine.this);
                vaccineList.size();
                intent = new Intent(AddVaccine.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }


    public ArrayList<ListItem> loadOwnerList() {
        ArrayList<ListItem> ownerList = new ArrayList<>();

        for (int i = 0; i < petOptions.size(); i++) {
            ownerList.add(new ListItem(petOptions.get(i).getId(), petOptions.get(i).getNombre()));
        }
        return ownerList;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Util.RESP_PET_LIST:
                if (resultCode == RESULT_OK) {
                    ListItem itemObject = (ListItem) data.getExtras().getSerializable("ITEM_LIST_ACTIVITY");
                    selectedPet = itemObject.getId();
                    txtPet.setText(itemObject.getValue());
                }
                break;


        }
    }

}
