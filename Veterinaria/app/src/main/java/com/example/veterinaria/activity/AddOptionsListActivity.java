package com.example.veterinaria.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.veterinaria.R;
import com.example.veterinaria.adapter.OptionListAdapter;
import com.example.veterinaria.models.ListItem;

import java.util.ArrayList;

public class AddOptionsListActivity extends AppCompatActivity {
    private ArrayList<ListItem> listItems;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_add_pet_options);
        initComponents();

    }


    public void initComponents() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            listItems = (ArrayList<ListItem>) bundle.get("LIST_ITEMS");
        }
        recyclerView = findViewById(R.id.recy_option_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(AddOptionsListActivity.this));
        OptionListAdapter optionListAdapter = new OptionListAdapter(listItems);
        optionListAdapter.setOnclickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("ITEM_LIST_ACTIVITY", listItems.get(recyclerView.getChildAdapterPosition(v)));
                setResult(AddOptionsListActivity.RESULT_OK, returnIntent);
                finish();
            }
        });
        recyclerView.setAdapter(optionListAdapter);

    }


}
