package com.example.veterinaria.DBO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.veterinaria.Util.Util;
import com.example.veterinaria.activity.MainActivity;
import com.example.veterinaria.models.Mascota;
import com.example.veterinaria.models.Propietario;
import com.example.veterinaria.models.Vacuna;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DbManager {


    public static void addOwner(String id, String name, String phoneNumber, Context context) {
        ConexionSqlHelper conexionSqlHelper = new ConexionSqlHelper(context, Util.bdName, null, 1);
        SQLiteDatabase sqLiteDatabase = conexionSqlHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("CEDULA", id);
        contentValues.put("NOMBRE", name);
        contentValues.put("TELEFONO", phoneNumber);
        Long result = sqLiteDatabase.insertOrThrow("PROPIETARIO", "ID", contentValues);
        sqLiteDatabase.close();
        Toast.makeText(context, "Usuario registrado con éxito", Toast.LENGTH_SHORT).show();
    }

    public static void addVaccine(String name, String date, double dose, int idMascota, Context context) {

        ConexionSqlHelper conexionSqlHelper = new ConexionSqlHelper(context, Util.bdName, null, 1);
        SQLiteDatabase sqLiteDatabase = conexionSqlHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("NOMBRE", name);
        contentValues.put("FECHA", date);
        contentValues.put("DOSIS", dose);
        contentValues.put("ID_MASCOTA", idMascota);

        Long result = sqLiteDatabase.insertOrThrow("VACUNA", "ID", contentValues);
        sqLiteDatabase.close();
        Toast.makeText(context, "Vacuna registrada con éxito", Toast.LENGTH_SHORT).show();


    }

    public static void deletePet(String id, Context context) {
        ConexionSqlHelper conexionSqlHelper = new ConexionSqlHelper(context, Util.bdName, null, 1);
        SQLiteDatabase sqLiteDatabase = conexionSqlHelper.getWritableDatabase();
        String query = "DELETE FROM MASCOTA  WHERE CEDULA='" + id + "' ";
        sqLiteDatabase.rawQuery(query, null);

        sqLiteDatabase.close();

        Toast.makeText(context, "Mascota registrada con éxito", Toast.LENGTH_SHORT).show();
    }

    public static void addPet(String id, String name, int type, int age, String race, int owner, Context context) {
        ConexionSqlHelper conexionSqlHelper = new ConexionSqlHelper(context, Util.bdName, null, 1);
        SQLiteDatabase sqLiteDatabase = conexionSqlHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("CEDULA", id);
        contentValues.put("NOMBRE", name);
        contentValues.put("TIPO", type);
        contentValues.put("EDAD", age);
        contentValues.put("RAZA", race);
        contentValues.put("ID_PROPIETARIO", owner);

        Long result = sqLiteDatabase.insertOrThrow("MASCOTA", "ID", contentValues);
        sqLiteDatabase.close();
        Toast.makeText(context, "Mascota registrada con éxito", Toast.LENGTH_SHORT).show();
    }

    public static ArrayList<Mascota> getPet(Context context) {
        ArrayList<Mascota> petList = new ArrayList<>();
        Mascota pet;
        ConexionSqlHelper conexionSqlHelper = new ConexionSqlHelper(context, Util.bdName, null, 1);
        SQLiteDatabase sqLiteDatabase = conexionSqlHelper.getReadableDatabase();
        String query = "SELECT * FROM MASCOTA";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                pet = new Mascota();
                pet.setId(cursor.getInt(cursor.getColumnIndex("ID")));
                pet.setCedula(cursor.getString(cursor.getColumnIndex("CEDULA")));
                pet.setNombre(cursor.getString(cursor.getColumnIndex("NOMBRE")));
                pet.setEdad(cursor.getInt(cursor.getColumnIndex("EDAD")));
                pet.setRaza(cursor.getString(cursor.getColumnIndex("RAZA")));
                pet.setIdPropietario(cursor.getInt(cursor.getColumnIndex("ID_PROPIETARIO")));
                petList.add(pet);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        sqLiteDatabase.close();
        return petList;

    }

    public static ArrayList<Vacuna> getVaccine(Context context) {
        ArrayList<Vacuna> vaccineList = new ArrayList<>();
        Vacuna vaccine;
        ConexionSqlHelper conexionSqlHelper = new ConexionSqlHelper(context, Util.bdName, null, 1);
        SQLiteDatabase sqLiteDatabase = conexionSqlHelper.getReadableDatabase();
        String query = "SELECT * FROM VACUNA";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                vaccine = new Vacuna();
                vaccine.setId(cursor.getInt(cursor.getColumnIndex("ID")));
                vaccine.setNombre(cursor.getString(cursor.getColumnIndex("NOMBRE")));
                vaccine.setFecha(cursor.getString(cursor.getColumnIndex("FECHA")));
                vaccine.setDosis(cursor.getDouble(cursor.getColumnIndex("DOSIS")));
                vaccine.setIdMascota(cursor.getInt(cursor.getColumnIndex("ID_MASCOTA")));
                vaccineList.add(vaccine);
            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
        sqLiteDatabase.close();
        return vaccineList;

    }


    public static String getPetInfo(Context context, String petId) {
        String info = "";
        ConexionSqlHelper conexionSqlHelper = new ConexionSqlHelper(context, Util.bdName, null, 1);
        SQLiteDatabase sqLiteDatabase = conexionSqlHelper.getReadableDatabase();
        String query = "SELECT M.NOMBRE NOMBRE_MASCOTA,M.TIPO TIPO_MASCOTA, " +
                "M.EDAD EDAD_MASCOTA, M.RAZA RAZA_MASCOTA, P.CEDULA CEDULA_PROPIETARIO, " +
                "P.NOMBRE NOMBRE_PROPIETARIO, P.TELEFONO TELEFONO_PROPIETARIO " +
                "FROM MASCOTA M JOIN PROPIETARIO P ON M.ID_PROPIETARIO = P.ID WHERE M.CEDULA = '" + petId + "'";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                info = "";
                info += "NOMBRE MASCOTA: " + cursor.getString(cursor.getColumnIndex("NOMBRE_MASCOTA")) + "\n";
                info += "TIPO MASCOTA: " + getPetType(cursor.getInt(cursor.getColumnIndex("TIPO_MASCOTA"))) + "\n";
                info += "RAZA MASCOTA: " + cursor.getString(cursor.getColumnIndex("RAZA_MASCOTA")) + "\n";
                info += "EDAD MASCOTA: " + cursor.getString(cursor.getColumnIndex("EDAD_MASCOTA")) + "\n";
                info += "CEDULA PROPIETARIO: " + cursor.getString(cursor.getColumnIndex("CEDULA_PROPIETARIO")) + "\n";
                info += "NOMBRE PROPIETARIO: " + cursor.getString(cursor.getColumnIndex("NOMBRE_PROPIETARIO")) + "\n";
                info += "TELEFONO PROPIETARIO: " + cursor.getString(cursor.getColumnIndex("TELEFONO_PROPIETARIO")) + "\n";

            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
        sqLiteDatabase.close();

        if (!info.equals("")) {
            return info;
        } else {

            return null;
        }

    }

    public static String getPetType(int id) {
        switch (id) {
            case 1:
                return "Perro";
            case 2:
                return "Gato";
            case 3:
                return "Pájaro";

            default:
                return "no aplica";

        }

    }

    public static ArrayList<Propietario> getOwner(Context context) {
        ArrayList<Propietario> ownerList = new ArrayList<>();
        Propietario owner;
        ConexionSqlHelper conexionSqlHelper = new ConexionSqlHelper(context, Util.bdName, null, 1);
        SQLiteDatabase sqLiteDatabase = conexionSqlHelper.getReadableDatabase();
        String query = "SELECT * FROM PROPIETARIO";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                owner = new Propietario();
                owner.setId(cursor.getInt(cursor.getColumnIndex("ID")));
                owner.setCedula(cursor.getString(cursor.getColumnIndex("CEDULA")));
                owner.setNombre(cursor.getString(cursor.getColumnIndex("NOMBRE")));
                owner.setTelefono(cursor.getString(cursor.getColumnIndex("TELEFONO")));
                ownerList.add(owner);
            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
        sqLiteDatabase.close();
        return ownerList;

    }

}
