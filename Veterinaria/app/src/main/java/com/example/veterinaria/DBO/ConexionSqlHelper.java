package com.example.veterinaria.DBO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.veterinaria.Util.Util;

public class ConexionSqlHelper extends SQLiteOpenHelper {


    public ConexionSqlHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Util.CREATE_MASCOTA);
        db.execSQL(Util.CREATE_PROPIETARIO);
        db.execSQL(Util.CREATE_VACUNA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS MASCOTA");
        db.execSQL("DROP TABLE IF EXISTS PROPIETARIO");
        db.execSQL("DROP TABLE IF EXISTS VACUNA");
        onCreate(db);
    }
}
